"""Power budget simulator."""
import argparse
import os.path

import matplotlib.pyplot as plt
import numpy as np

import solar_power_budget as spb
from battery_simulation import BatterySimulation
from config import axis_labels, read_config
from keplerians import create_tle
from load_simulation import SystemPowerAnalyzer

# Parameters
axis_colors = [
    'red', 'darkred', 'limegreen', 'darkgreen', 'cornflowerblue', 'mediumblue'
]


def plot_stacked(plot,
                 title,
                 TLE,
                 values,
                 skip_plot_axis,
                 mean_power,
                 timestep=1,
                 interval=60,
                 xlabel='Time (min)'):
    """Create stacked axis plot."""
    plot.figure(title, figsize=(8, 5))
    plot.xlabel(xlabel)
    plot.ylabel('Power (mW)')
    plot.title('\n'.join(TLE), loc='left')
    # Set X axis values
    time_values = np.arange(len(values[0])) * timestep / interval

    for axis, side in enumerate(values):
        if axis not in skip_plot_axis:
            plot.plot(time_values,
                      side,
                      axis_colors[axis],
                      label=axis_labels[axis] + ': ' + f'{min(side):.1f}' +
                      f'/{max(side):.1f}' + f'/{mean_power[axis]:.1f} mW')
    plot.legend(title='Power (min/max/mean)', loc='upper right')


def plot_split(plot,
               title,
               TLE,
               side_power_values,
               skip_plot_axis,
               mean_power,
               timestep=1,
               interval=60,
               xlabel='Time (min)'):
    """Create split axis plot."""
    fig, axs = plot.subplots(6 - len(skip_plot_axis),
                             sharex=True,
                             sharey=True,
                             num=title,
                             figsize=(8, 9))
    fig.suptitle('\n'.join(TLE))
    fig.text(0.5, 0.04, 'Time (min)', ha='center')
    fig.text(0.04, 0.5, 'Power (mW)', va='center', rotation='vertical')
    # Set X axis values
    time_values = np.arange(len(side_power_values[0])) * timestep / interval
    idx = 0
    for axis, side in enumerate(side_power_values):
        if axis not in skip_plot_axis:
            axs[idx].plot(time_values,
                          side,
                          axis_colors[axis],
                          label=axis_labels[axis] + ': ' + f'{min(side):.1f}' +
                          f'/{max(side):.1f}' + f'/{mean_power[axis]:.1f} mW')
            axs[idx].legend(title='Power (min/max/mean)', loc='upper right')
            idx += 1


# Calculation
def generate_plots(sim_params, parameters):
    """Run simulation and plot results."""
    for name, spacecraft in sim_params['spacecrafts']:
        # set duration
        if sim_params['duration']:
            sim_params['orbit_count'] = sim_params['duration'] * \
                24 * 60 / spb.get_orbit_period(spacecraft)
            orbit_duration_txt = str(sim_params['duration']) + 'd'
        else:
            orbit_duration_txt = f"{sim_params['orbit_count']:.1f}"
        if (len(sim_params['skip_plot_axis']) > 0):
            print(
                'Not plotting the following axis:',
                ','.join(axis_labels[a] for a in sim_params['skip_plot_axis']))
        if sim_params['output_path']:
            filename_prefix = os.path.join(sim_params['output_path'],
                                           f'{name}_{orbit_duration_txt}')
        else:
            filename_prefix = ''

        # Start simulation
        print('------------------', name, '------------------')
        print('Orbit period:', spb.get_orbit_period(spacecraft), 'min')
        print('Orbit count: ', sim_params['orbit_count'])
        if sim_params['SolarLockY']:
            sim_params['ang_v'][1] = spb.get_sat_frame_angV(spacecraft)
            # print(sim_params['ang_v'])

        # TEST CLASS Solar Cells
        # solar_panels = SolarCells({'pv_count': sim_params['pv_count'],
        #             'pv_eff': sim_params['pv_eff'],
        #             'pv_area': sim_params['pv_area'],
        #             'pv_pwr': sim_params['pv_pwr'], })

        mean_p_coeff, mean_power, side_power_values, orbit_side_mean_power, \
            eclipse_pc = spb.power_calculation(spacecraft,
                                               sim_params['JD_ini'],
                                               sim_params['compute_albedo'],
                                               sim_params['orbit_count'],
                                               sim_params['ang_v'],
                                               sim_params['pose0'],
                                               sim_params['pv_count'],
                                               sim_params['pv_eff'],
                                               sim_params['pv_area'],
                                               sim_params['pv_pwr'],
                                               timestep=sim_params['timestep'],
                                               show_progress=True)
        # Calculate Power with EPS efficiency
        side_power_values_eff = [
            element * parameters['Spacecraft']['EPS']['charge_efficiency']
            for element in side_power_values
        ]
        # Show eclipse percentage
        print('\nEclipse percentage (min/max)', f'{min(eclipse_pc)*100:.1f}',
              f'{max(eclipse_pc)*100:.1f}')
        # Mean power coefficient
        print('Mean Power coefficient of each side')
        for axis, cf in tuple(zip(axis_labels, mean_p_coeff)):
            print(axis, cf)
        print('Total Mean Power coefficient', sum(mean_p_coeff))
        print()
        # Mean power
        print('OAP of each side')
        for axis, cf in tuple(zip(axis_labels, mean_power)):
            print(axis, cf)
        print('Total OAP in mW', mean_power.sum(), 'mW')

        # Plot power
        print('Generating plots')
        if parameters['Sim'].get(
                'plot_power_in') is None or parameters['Sim']['plot_power_in']:
            if parameters['Sim']['stack_axis'] or len(
                    sim_params['skip_plot_axis']) == 5:
                plot_stacked(plt, name + ' ' + orbit_duration_txt,
                             create_tle(spacecraft), side_power_values_eff,
                             sim_params['skip_plot_axis'], mean_power,
                             sim_params['timestep'])
            else:
                plot_split(plt, name + ' ' + orbit_duration_txt,
                           create_tle(spacecraft), side_power_values_eff,
                           sim_params['skip_plot_axis'], mean_power,
                           sim_params['timestep'])
            if sim_params['output_path']:
                plt.savefig(
                    os.path.join(filename_prefix + '.svg'))
                plt.savefig(
                    os.path.join(filename_prefix + '.png'))

        # Plot power per orbit
        if parameters['Sim'].get(
                'plot_oap') is None or parameters['Sim']['plot_oap']:
            plot_stacked(plt,
                         'OAP ' + name + orbit_duration_txt,
                         create_tle(spacecraft),
                         orbit_side_mean_power,
                         sim_params['skip_plot_axis'],
                         mean_power,
                         interval=1,
                         xlabel='Orbit')
            if sim_params['output_path']:
                plt.savefig(
                    os.path.join(filename_prefix + ' OAP.svg'))
                plt.savefig(
                    os.path.join(filename_prefix + ' OAP.png'))

        # Calculate and plot battery charge
        cycles = parameters['Sim'].get('load_sim_cycles') or 1
        analyzer = SystemPowerAnalyzer(parameters['Spacecraft']['loads'],
                                       cycles=cycles)

        if parameters['Sim'].get(
                'plot_battery') is None or parameters['Sim']['plot_battery']:
            batsim = BatterySimulation(
                capacity_mwh=parameters['Spacecraft']['battery']['capacity'],
                charge_current_limit=parameters['Spacecraft']['battery']
                ['charge_current_limit'],
                nominal_voltage=parameters['Spacecraft']['battery']
                ['nominal_voltage'])
            if isinstance(
                    parameters['Spacecraft']['battery']['initial_charge'],
                    int):
                bat_ini_charge = parameters['Spacecraft']['battery'][
                    'initial_charge']
            else:
                bat_ini_charge = parameters['Spacecraft']['battery'][
                    'capacity'] * float(parameters['Spacecraft']['battery']
                                        ['initial_charge'].rstrip('%')) / 100
            charge_count = len(side_power_values_eff[0])

            consumption = []
            for t in range(0, charge_count):
                t1 = t * sim_params['timestep']
                t2 = t1 + sim_params['timestep']
                consumption.append(analyzer.calculate_average_power(t1, t2))
            batsim.simulate(consumption,
                            side_power_values_eff,
                            sim_params['timestep'],
                            initial_charge_mwh=bat_ini_charge)
            bch_plt = batsim.plot_battery_charge(name)
            if sim_params['output_path']:
                bch_plt.savefig(
                    os.path.join(filename_prefix + ' Battery Charge.svg'))
                bch_plt.savefig(
                    os.path.join(filename_prefix + ' Battery Charge.png'))
            bcu_plt = batsim.plot_battery_current(name)
            if sim_params['output_path']:
                bcu_plt.savefig(
                    os.path.join(filename_prefix + ' Battery Current.svg'))
                bcu_plt.savefig(
                    os.path.join(filename_prefix + ' Battery Current.png'))
        # Plot loads
        if (parameters['Sim'].get('plot_system_loads') is None
                or parameters['Sim']['plot_system_loads']):
            sl_plt = analyzer.plot_systems_loads(name)
            if sim_params['output_path']:
                sl_plt.savefig(
                    os.path.join(filename_prefix + ' System Loads.svg'))
                sl_plt.savefig(
                    os.path.join(filename_prefix + ' System Loads.png'))
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c',
                        '--conf',
                        dest='conf_file',
                        help='Configuration file',
                        default='parameters.yaml',
                        action='store',
                        required=False)
    args = parser.parse_args()
    print('Loading parameters from', args.conf_file)
    s_params, params = read_config(args.conf_file)
    generate_plots(s_params, params)
