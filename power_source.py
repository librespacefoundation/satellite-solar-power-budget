class SolarCells:
    def __init__(self, solar_cells, si=1.361):
        self.SI = si
        # Calculate power capability per axis
        if solar_cells['pv_pwr'] is None:
            self.pv_panels_power = si * solar_cells['pv_area'] * solar_cells['pv_eff'] * solar_cells['pv_count']  # noqa: E501
        else:
            self.pv_panels_power = solar_cells['pv_pwr'] * solar_cells['pv_count']  # noqa: E501

    def get_pv_panel_power(self):
        return self.pv_panels_power
