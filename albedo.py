import numpy as np
import pandas as pd
from astropy import units as u
from astropy.coordinates import (ITRS, TEME, CartesianRepresentation,
                                 EarthLocation)
from astropy.time import Time


def teme_to_lla(r_sat, JD):
    """Converts coordinates from thw TEME frame to latitude-longitude-altitude
        r_sat must be given in km"""
    t = Time(JD, format='jd')
    teme_p = CartesianRepresentation(r_sat*u.km)
    teme = TEME(teme_p, obstime=t)
    itrs_geo = teme.transform_to(ITRS(obstime=t))
    location = itrs_geo.earth_location
    location.geodetic.lon.wrap_angle = 180*u.deg  # wrap lon. in [-180, 180]
    return np.array([location.geodetic.lat.value,
                     location.geodetic.lon.value,
                     location.geodetic.height.value])


def idx_to_angle(row_idx, col_idx, n_rows, n_cols):
    """Converts the indices of the TOMS data to grid cell angles"""
    dx = 2 * np.pi/n_cols
    dy = np.pi / n_rows

    lat = np.rad2deg(np.pi-dy/2 - row_idx*dy)
    lon = np.rad2deg(col_idx*dx - np.pi + dx/2)
    return lat, lon


def in_fov_3(r_sat: np.ndarray, r_cell: np.ndarray, r_body=6371.0) -> bool:
    # calculate the angle between the cell and the satellite position vectors
    ang_sat_cell = np.arccos(np.dot(r_sat, r_cell) /
                             np.linalg.norm(r_cell) /
                             np.linalg.norm(r_sat))

    # Determine whether the cell is at the other side of the earth
    if (ang_sat_cell >= np.pi/2 or ang_sat_cell <= -np.pi/2):
        return False
    else:
        # Calculate the distance between the tangent point and the satellite
        # using the Pythagorean theorem
        r_tang_sat = np.sqrt(np.linalg.norm(r_sat)**2 - r_body**2)

        # Calculate the max FOV angle
        ang_max_fov = np.arctan(r_body/r_tang_sat)

        # Calculate the angle between r_sat and r_body
        # (the triange they form is a right one)
        ang_sat_body = np.pi/2 - ang_max_fov

        if (ang_sat_cell <= ang_sat_body):
            return True
        else:
            return False


def cell_to_obj_angle(r_sat, r_cell):
    """Calculates the angle between the normal vector of a grid cell
        and a celestial object"""

    r_cell = np.array(r_cell)
    r_sat = np.array(r_sat)
    r_sat_cell = r_sat - r_cell

    angle = np.pi - np.arccos(np.dot(-r_cell, r_sat_cell) /
                              np.linalg.norm(-r_cell) /
                              np.linalg.norm(r_sat_cell))
    return angle


def earth_cell_area(cell_lat, cell_lon, lat_step, lon_step, r_earth=6371.0):
    """Returns the area of a patch on earth surface in [km^2]

    :param cell_lat: _description_
    :type cell_lat: _type_
    :param cell_lon: _description_
    :type cell_lon: _type_
    :param lat_step: _description_
    :type lat_step: _type_
    :param lon_step: _description_
    :type lon_step: _type_
    :param r_earth: _description_, defaults to 6378
    :type r_earth: float, optional
    :return: _description_
    :rtype: _type_
    """
    A = np.deg2rad(lon_step)*r_earth**2*(
        np.cos(np.deg2rad(cell_lat - lat_step/2))
        - np.cos(np.deg2rad(cell_lat + lat_step/2)))

    return A


def albedo_coeff(JD, r_sat, r_sun, R_bl, R_le, panels: np.ndarray,
                 r_earth=6371.0):
    """Calculates the albedo power coefficient, which is the albedo power,
    divided by the solar constant.

    :param JD: Julian date
    :type JD: _type_
    :param r_sat: Satellite position in the TEME frame
    :type r_sat: _type_
    :param r_sun: Sun position in the TEME frame
    :type r_sun: _type_
    :param R_bl: Rotation matrix from the body frame to the LVLH frame
    :type R_bl: _type_
    :param R_le: Rotation matrix from the LVLH to the TEME frame
    :type R_le: _type_
    :param panels: nx3 array containing the normal vectors of
    the solar panels, expressed in the satellite's body frames.
    The normal vectors are considered to be pointing "outwards".
    :type panels: np.ndarray
    """

    # Define time
    t = Time(JD, format='jd')

    # Import reflectivity dataset
    refl = np.array(pd.read_csv('albedo_data/Earth_ALB_2018_CERES_All_5x5.csv',
                    header=None))
    lat_step = 5.0  # deg
    lon_step = 5.0  # deg

    albedo_power_panels = np.zeros([panels.shape[0], 1])

    # Convert latitude-longitude pairs to vectors in the TEME frame
    n_cells = refl.shape[0] * refl.shape[1]
    astropy_lats = np.zeros(n_cells)
    astropy_lons = np.zeros(n_cells)

    for i in range(refl.shape[0]):
        for j in range(refl.shape[1]):
            nasa_lat, nasa_lon = idx_to_angle(i, j,
                                              refl.shape[0], refl.shape[1])
            # Astropy latitude range: [-90, 90].
            # It makes no difference for astropy if longitude is
            # provided in [0,360] or [-180, 180].
            astropy_lats[i*refl.shape[1] + j] = nasa_lat - 90
            astropy_lons[i*refl.shape[1] + j] = nasa_lon

    # cells are at the surface of the Earth, so alt=0
    alts = np.zeros(refl.shape[0] * refl.shape[1])
    nc_geo = EarthLocation.from_geodetic(astropy_lons, astropy_lats, alts)
    nc_itrs = nc_geo.get_itrs(obstime=t)
    nc_teme_matrix = nc_itrs.transform_to(TEME(obstime=t))

    in_fov_count = 0
    total_area = 0

    # Iterate through the grid cells and calculate the albedo power
    for i in range(refl.shape[0]):
        for j in range(refl.shape[1]):
            if refl[i, j] == 99999.0:
                continue

            # Calculate cell latitude and longitude
            cell_lat, cell_lon = idx_to_angle(i, j,
                                              refl.shape[0], refl.shape[1])

            # Get the position vector of the cell in the TEME frame (in km)
            cell_teme = nc_teme_matrix[i * refl.shape[1] + j].cartesian.xyz.value / 1000  # NOQA: E501

            # Calculate panel vectors in the TEME frame
            panels_teme = np.zeros(panels.shape)
            for k in range(panels.shape[0]):
                panel_lvlh = R_bl.transpose() @ panels[k, :]
                panels_teme[k, :] = R_le.transpose() @ panel_lvlh

            # Calculate cell surface area (use NASA lat-lon)
            A_c = earth_cell_area(cell_lat, cell_lon, lat_step, lon_step)

            # Determine if the cell belongs to the satellite's & Sun's FOV
            cell_in_sat_fov = in_fov_3(r_sat, cell_teme)
            cell_in_sun_fov = in_fov_3(r_sun, cell_teme)

            # Compute the vector between the cell and the satellite
            # with direction towards the cell.
            r_sat_cell = -(r_sat - cell_teme)
            n_r_sat_cell = r_sat_cell / np.linalg.norm(r_sat_cell)

            # For the cells visible by the sun and the satellite, calculate
            # the albedo power per satellite side.
            if (cell_in_sat_fov is True and cell_in_sun_fov is True):
                # Log some debugging metrics
                total_area += A_c
                in_fov_count += 1

                # Calculate the angle between the cell normal and the sun
                sun_ang = cell_to_obj_angle(r_sun, cell_teme)
                if sun_ang > np.pi / 2:
                    sun_ang = np.pi / 2
                if sun_ang < -np.pi / 2:
                    sun_ang = -np.pi / 2

                # Calculate the angle between the cell normal and the satellite
                sat_ang = cell_to_obj_angle(r_sat, cell_teme)
                if sat_ang > np.pi / 2:
                    sat_ang = np.pi / 2
                if sat_ang < -np.pi / 2:
                    sat_ang = -np.pi / 2

                for k in range(0, panels.shape[0], 1):
                    tmp = np.dot(panels_teme[k, :], n_r_sat_cell)
                    if tmp >= 0:
                        albedo_power_panels[k] += (refl[i, j] *
                                                   A_c *
                                                   np.cos(sun_ang) *
                                                   np.cos(sat_ang) *
                                                   tmp /
                                                   np.pi /
                                                   np.linalg.norm(cell_teme - r_sat)**2)  # NOQA: E501

    # Print some debugging metrics
    # print(f"Elements in FOV : {in_fov_count} \n")
    # print(f"Albedo power : {albedo_power_panel[0]}")
    # print(f"Total area : {total_area}")

    return albedo_power_panels


if __name__ == '__main__':
    pass
