import matplotlib.pyplot as plt
import numpy as np


class BatterySimulation:

    def __init__(self, capacity_mwh, charge_current_limit, nominal_voltage):
        self.capacity_mwh = capacity_mwh
        self.charge_limit_mw = charge_current_limit * nominal_voltage
        # print(f'Charge power limit: {self.charge_limit_mw}mW')
        self.charge_current_limit = charge_current_limit
        self.battery_charge_mwh = []
        self.timestep = 1
        self.nominal_voltage = nominal_voltage
        self.battery_current = []

    def simulate(self,
                 consumption,
                 charging_source,
                 step_interval_s,
                 initial_charge_mwh=0):
        self.timestep = step_interval_s
        capacity_mws = self.capacity_mwh * 3600
        current_charge_mws = initial_charge_mwh * 3600
        self.battery_charge_mwh.append(current_charge_mws / 3600)

        charge_count = len(charging_source[0])

        for i in range(charge_count):
            charge_step = [source[i] for source in charging_source]
            power_added_mw = min((sum(charge_step) - consumption[i]),
                                 self.charge_limit_mw)
            current_charge_mws += power_added_mw * step_interval_s
            charge_current = min(
                self.charge_current_limit,
                ((sum(charge_step) - consumption[i]) / self.nominal_voltage))
            self.battery_current.append(charge_current)

            # Ensure charge does not exceed capacity and is not negative
            current_charge_mws = max(0, min(current_charge_mws, capacity_mws))

            self.battery_charge_mwh.append(current_charge_mws / 3600)

        return self.battery_charge_mwh

    def plot_battery_charge(self, plot_name=''):
        fig, ax = plt.subplots(num=f'{plot_name} Battery charge')
        fig.set_size_inches(8, 5)
        plt.title('Battery charge - Capacity: %d mWh' % (self.capacity_mwh))
        plt.xlabel('Time (min)')
        plt.ylabel('Charge (mWh)')
        plt.grid(True)

        time_values = np.arange(len(
            self.battery_charge_mwh)) * self.timestep / 60
        ax.plot(time_values, self.battery_charge_mwh, label='Battery charge')
        capacity_threshold_mwh = 20 * self.capacity_mwh / 100
        ax.plot([0, max(time_values)],
                [capacity_threshold_mwh, capacity_threshold_mwh],
                'orange',
                label='Capacity threshold')
        ax.legend()

        # secondary axis for %
        ax2 = ax.twinx()
        ax2.set_label('Charge %')

        def capacity_pc(C_mwh):
            return 100 * C_mwh / self.capacity_mwh

        # C_perc = lambda C_mwh: 100 * C_mwh / self.capacity_mwh
        ymin, ymax = ax.get_ylim()
        ax2.set_ylim((capacity_pc(ymin), capacity_pc(ymax)))
        ax2.plot([], [])
        return plt

    def plot_battery_current(self, plot_name=''):
        fig, ax = plt.subplots(num=f'{plot_name} Battery current')
        fig.set_size_inches(8, 5)
        plt.title('Battery current - Capacity: %d mWh' % (self.capacity_mwh))
        plt.xlabel('Time (min)')
        plt.ylabel('Current (mA)')
        plt.grid(True)
        plt.axhline(y=self.charge_current_limit,
                    color='red',
                    linestyle='--',
                    label='Battery charge current limit')
        # plt.axhline(y=2600, color='orange', linestyle='--',
        # label='Nominal charge current')

        time_values = np.arange(len(self.battery_current)) * self.timestep / 60
        ax.plot(time_values, self.battery_current, label='Battery current')
        ax.legend()
        return plt


if __name__ == '__main__':
    # Create an instance of the BatterySimulation class
    simulation = BatterySimulation(100, 10, 3.7)

    # Define the loads as tuples
    loads = [(100, 10, 20)]

    # Define the charging source array (6 x n)
    charging_source = [[
        0, 0, 0, 0, 100, 100, 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ], [0] * 20, [0] * 20, [0] * 20, [0] * 20, [0] * 20]

    # Define the step interval
    step_interval_s = 180

    # Run the simulation with an initial charge of 100Wh
    battery_charge = simulation.simulate(loads,
                                         charging_source,
                                         step_interval_s,
                                         initial_charge_mwh=100)

    # Print the battery charge array
    print(battery_charge)
