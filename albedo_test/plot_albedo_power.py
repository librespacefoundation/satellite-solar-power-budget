import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

data = pd.read_csv("albedo_test/albedo_power_panels.csv", header=None)
data = np.array(data)

data_bsk = pd.read_csv("albedo_test/albedo_power_basilisk.csv", header=None)
data_bsk = np.array(data_bsk)

timestep = 60  # sec
times = np.arange(0, timestep*data.shape[1], timestep)

timestep_bsk = 10  # sec
times_bsk = np.arange(0, timestep_bsk*data_bsk.shape[1], timestep_bsk)

# Plot albedo flux
fig, ((ax0, ax1), (ax2, ax3), (ax4, ax5)) = plt.subplots(3, 2)

l_0, = ax0.plot(times, data[0], label="+X")
l_0_b, = ax0.plot(times_bsk, data_bsk[0], label="+X-bsk")

l_1, = ax1.plot(times, data[1], label="-X")
l_1_b, = ax1.plot(times_bsk, data_bsk[1], label="-X-bsk")

l_2, = ax2.plot(times, data[2], label="+Y")
l_2_b, = ax2.plot(times_bsk, data_bsk[2], label="+Y-bsk")

l_3, = ax3.plot(times, data[3], label="-Y")
l_3_b, = ax3.plot(times_bsk, data_bsk[3], label="-Y-bsk")

l_4, = ax4.plot(times, data[4], label="+Z")
l_4_b, = ax4.plot(times_bsk, data_bsk[4], label="+Z-bsk")

l_5, = ax5.plot(times, data[5], label="-Z")
l_5_b, = ax5.plot(times_bsk, data_bsk[5], label="-Z-bsk")

ax0.legend(handles=[l_0, l_0_b])
ax1.legend(handles=[l_1, l_1_b])
ax2.legend(handles=[l_2, l_2_b])
ax3.legend(handles=[l_3, l_3_b])
ax4.legend(handles=[l_4, l_4_b])
ax5.legend(handles=[l_5, l_5_b])
fig.suptitle("Albedo power per side (W/m^2)")

fig2, ax6 = plt.subplots()

l_6, = ax6.plot(times, np.sum(data, axis=0), c="darkgreen", label="LSF")
l_6_b, = ax6.plot(times_bsk, np.sum(data_bsk, axis=0), c="firebrick",
                  label="Basilisk")
ax6.legend(handles=[l_6, l_6_b])
ax6.set_title("Total albedo power [W/m^2]")
ax6.set_xlabel("Time [s]")

plt.show()
