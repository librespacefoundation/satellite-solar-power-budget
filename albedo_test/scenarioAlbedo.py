#
#  ISC License
#
#  Copyright (c) 2020, Autonomous Vehicle Systems Lab, University of Colorado at Boulder
#
#  Permission to use, copy, modify, and/or distribute this software for any
#  purpose with or without fee is hereby granted, provided that the above
#  copyright notice and this permission notice appear in all copies.
#
#  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
#  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
#  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
#  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
#  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
#  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
#

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# The path to the location of Basilisk
# Used to get the location of supporting data.
from Basilisk import __path__
# import message declarations
from Basilisk.architecture import messaging
from Basilisk.fswAlgorithms import attTrackingError, hillPoint, mrpFeedback
# import simulation related support
from Basilisk.simulation import (albedo, coarseSunSensor, eclipse,
                                 extForceTorque, simpleNav, spacecraft)
# import general simulation support files
from Basilisk.utilities import \
    unitTestSupport  # general support file with common unit test functions
from Basilisk.utilities import RigidBodyKinematics, SimulationBaseClass, macros
from Basilisk.utilities import orbitalMotion as om
from Basilisk.utilities import simIncludeGravBody

bskPath = __path__[0]
fileNameString = os.path.basename(os.path.splitext(__file__)[0])


def run(show_plots, albedoData, multipleInstrument, multiplePlanet, useEclipse, simTimeStep):
    """
    At the end of the python script you can specify the following example parameters.

    Args:
        show_plots (bool): Determines if the script should display plots.
        albedoData (bool): Flag indicating if the albedo data based model should be used.
        multipleInstrument (bool): Flag indicating if multiple instrument should be used.
        multiplePlanet (bool): Flag specifying if multiple planets should be used.
        useEclipse (bool): Flag indicating if the partial eclipse at the incremental area is considered.
        simTimeStep (double): Flag specifying the simulation time step.

    """

    # Create simulation variable names
    simTaskName = "simTask"
    simProcessName = "simProcess"
    # Create a sim module as an empty container
    scSim = SimulationBaseClass.SimBaseClass()
    # Create the simulation process
    dynProcess = scSim.CreateNewProcess(simProcessName)
    # Create the dynamics task
    if simTimeStep is None:
        simulationTimeStep = macros.sec2nano(10.)
    else:
        simulationTimeStep = macros.sec2nano(simTimeStep)
    dynProcess.addTask(scSim.CreateNewTask(simTaskName, simulationTimeStep))
    # Create sun message
    sunPositionMsg = messaging.SpicePlanetStateMsgPayload()
    # sunPositionMsg.PositionVector = [-om.AU * 1000., 0.0, 0.0]
    sunPositionMsg.PositionVector = np.array([37620343.65348968,-130492745.82156882,-56582561.73571112]) * 1000
    sunMsg = messaging.SpicePlanetStateMsg().write(sunPositionMsg)

    # Create planet message (earth)
    gravFactory = simIncludeGravBody.gravBodyFactory()
    # Create planet message (earth)
    planetCase1 = 'earth'
    planet1 = gravFactory.createEarth()
    planet1.isCentralBody = True  # ensure this is the central gravitational body
    planet1.useSphericalHarmonicsGravityModel(bskPath+'/supportData/LocalGravData/GGM03S-J2-only.txt', 2)
    req1 = planet1.radEquator

    planetPositionMsg1 = messaging.SpicePlanetStateMsgPayload()
    planetPositionMsg1.PositionVector = [0., 0., 0.]
    planetPositionMsg1.PlanetName = planetCase1
    planetPositionMsg1.J20002Pfix = np.identity(3)
    pl1Msg = messaging.SpicePlanetStateMsg().write(planetPositionMsg1)

    #
    # Initialize spacecraft object and set properties
    #
    oe = om.ClassicElements()
    scObject = spacecraft.Spacecraft()
    scObject.ModelTag = "bsk-Sat"
    # Define the simulation inertia
    I = [900., 0., 0.,
         0., 800., 0.,
         0., 0., 600.]
    scObject.hub.mHub = 750.0  # kg - spacecraft mass
    scObject.hub.r_BcB_B = [[0.0], [0.0], [0.0]]  # m - position vector of body-fixed point B relative to CM
    scObject.hub.IHubPntBc_B = unitTestSupport.np2EigenMatrix3d(I)

    # Single planet case (earth)
    oe.a = 6903.13*1000
    oe.e = 0.000144
    oe.i = 97.5 * macros.D2R
    oe.Omega = 75.0 * macros.D2R
    oe.omega = 0.0 * macros.D2R
    oe.f = 0.0 * macros.D2R
    rN, vN = om.elem2rv(planet1.mu, oe)
    # set the simulation time
    n = np.sqrt(planet1.mu / oe.a / oe.a / oe.a)
    P = 2. * np.pi / n
    simulationTime = macros.sec2nano(1.0 * P)

    # Set initial spacecraft states
    rN_from_sgp4 = np.array([1785.3252109166965,6670.4159450684265,-14.717625099998317]) * 1000
    vN_from_sgp4 = np.array([0.9596654900164151,-0.2486691054662694,7.535095914108153]) * 1000

    # scObject.hub.r_CN_NInit = rN  # m - r_CN_N
    # scObject.hub.v_CN_NInit = vN  # m - v_CN_N
    scObject.hub.r_CN_NInit = rN_from_sgp4  # m - r_CN_N
    scObject.hub.v_CN_NInit = vN_from_sgp4  # m - v_CN_N
    scObject.hub.sigma_BNInit = [[0.0], [0.0], [0.0]]  # sigma_BN_B
    scObject.hub.omega_BN_BInit = [[0.0], [0.0], [0.0 * macros.D2R]]  # rad/s - omega_BN_B
    gravFactory.addBodiesTo(scObject)

    # Add spacecraft object to the simulation process
    scSim.AddModelToTask(simTaskName, scObject)

    # setup extForceTorque module
    # the control torque is read in through the messaging system
    extFTObject = extForceTorque.ExtForceTorque()
    extFTObject.ModelTag = "externalDisturbance"
    # use the input flag to determine which external torque should be applied
    # Note that all variables are initialized to zero.  Thus, not setting this
    # vector would leave it's components all zero for the simulation.
    scObject.addDynamicEffector(extFTObject)
    scSim.AddModelToTask(simTaskName, extFTObject)

    # add the simple Navigation sensor module.  This sets the SC attitude, rate, position
    # velocity navigation message
    sNavObject = simpleNav.SimpleNav()
    sNavObject.ModelTag = "SimpleNavigation"
    scSim.AddModelToTask(simTaskName, sNavObject)
    sNavObject.scStateInMsg.subscribeTo(scObject.scStateOutMsg)

    # setup hillPoint guidance module
    attGuidance = hillPoint.hillPoint()
    attGuidance.ModelTag = "hillPoint"
    attGuidance.transNavInMsg.subscribeTo(sNavObject.transOutMsg)
    # if you want to connect attGuidance.celBodyInMsg, then you need a planet ephemeris message of
    # type EphemerisMsgPayload.  In this simulation the input message is not connected to create an empty planet
    # ephemeris message which puts the earth at (0,0,0) origin with zero speed.
    CelBodyData = messaging.EphemerisMsgPayload() # make zero'd planet ephemeris message
    celBodyInMsg = messaging.EphemerisMsg().write(CelBodyData)
    attGuidance.celBodyInMsg.subscribeTo(celBodyInMsg)
    scSim.AddModelToTask(simTaskName, attGuidance)


    # setup the attitude tracking error evaluation module
    attError = attTrackingError.attTrackingError()
    attError.ModelTag = "attErrorInertial3D"
    scSim.AddModelToTask(simTaskName, attError)
    # if useAltBodyFrame:
        # attError.sigma_R0R = [0, 1, 0]
    attError.attRefInMsg.subscribeTo(attGuidance.attRefOutMsg)
    attError.attNavInMsg.subscribeTo(sNavObject.attOutMsg)

    # setup the MRP Feedback control module
    mrpControl = mrpFeedback.mrpFeedback()
    mrpControl.ModelTag = "mrpFeedback"
    scSim.AddModelToTask(simTaskName, mrpControl)
    mrpControl.guidInMsg.subscribeTo(attError.attGuidOutMsg)
    mrpControl.K = 3.5
    mrpControl.Ki = -1.0  # make value negative to turn off integral feedback
    mrpControl.P = 30.0
    mrpControl.integralLimit = 2. / mrpControl.Ki * 0.1

    # connect torque command to external torque effector
    extFTObject.cmdTorqueInMsg.subscribeTo(mrpControl.cmdTorqueOutMsg)

    # create the FSW vehicle configuration message
    vehicleConfigOut = messaging.VehicleConfigMsgPayload()
    vehicleConfigOut.ISCPntB_B = I  # use the same inertia in the FSW algorithm as in the simulation
    configDataMsg = messaging.VehicleConfigMsg().write(vehicleConfigOut)
    mrpControl.vehConfigInMsg.subscribeTo(configDataMsg)

    #
    # Albedo Module
    #
    albModule = albedo.Albedo()
    albModule.ModelTag = "AlbedoModule"
    albModule.spacecraftStateInMsg.subscribeTo(scObject.scStateOutMsg)
    albModule.sunPositionInMsg.subscribeTo(sunMsg)

    if useEclipse:
        albModule.eclipseCase = True
        eclipseObject = eclipse.Eclipse()
        eclipseObject.sunInMsg.subscribeTo(sunMsg)
        eclipseObject.addSpacecraftToModel(scObject.scStateOutMsg)
        eclipseObject.addPlanetToModel(pl1Msg)
        scSim.AddModelToTask(simTaskName, eclipseObject)

    def setupCSS(CSS, nHat_B: np.ndarray):
        CSS.stateInMsg.subscribeTo(scObject.scStateOutMsg)
        CSS.sunInMsg.subscribeTo(sunMsg)
        CSS.fov = 90. * macros.D2R
        CSS.maxOutput = 1.0
        CSS.nHat_B = nHat_B
        if useEclipse:
            CSS.sunEclipseInMsg.subscribeTo(eclipseObject.eclipseOutMsgs[0])

    #
    # CSS setup
    #
    n_CSS = 6  # one per spacecraft side

    CSS1 = coarseSunSensor.CoarseSunSensor()
    CSS1.ModelTag = "CSS1"
    setupCSS(CSS1, np.array([1.0, 0.0, 0.0]))

    CSS2 = coarseSunSensor.CoarseSunSensor()
    CSS2.ModelTag = "CSS2"
    setupCSS(CSS2, np.array([-1.0, 0.0, 0.0]))

    CSS3 = coarseSunSensor.CoarseSunSensor()
    CSS3.ModelTag = "CSS3"
    setupCSS(CSS3, np.array([0.0, 1.0, 0.0]))

    CSS4 = coarseSunSensor.CoarseSunSensor()
    CSS4.ModelTag = "CSS4"
    setupCSS(CSS4, np.array([0.0, -1.0, 0.0]))

    CSS5 = coarseSunSensor.CoarseSunSensor()
    CSS5.ModelTag = "CSS5"
    setupCSS(CSS5, np.array([0.0, 0.0, 1.0]))

    CSS6 = coarseSunSensor.CoarseSunSensor()
    CSS6.ModelTag = "CSS6"
    setupCSS(CSS6, np.array([0.0, 0.0, -1.0]))

    # Side order is considered as +X, -X, +Y, -Y, +Z, -Z
    # The CSSs have been assigned to axes so that the results
    # match the axes of the solar power budget tool. To see the Hill frame
    # axes considered by basilisk check scr/architecture/utilities/orbitalMotion.c
    CSS = [CSS6, CSS5, CSS3, CSS4, CSS2, CSS1]

    if albedoData:
        dataPath = os.path.abspath(bskPath + "/supportData/AlbedoData/")
        fileName = "Earth_ALB_2018_CERES_All_5x5.csv"
        albModule.addPlanetandAlbedoDataModel(pl1Msg, dataPath, fileName)
    else:
        ALB_avg = 0.5
        numLat = 200
        numLon = 200
        albModule.addPlanetandAlbedoAverageModel(pl1Msg, ALB_avg, numLat, numLon)
    #
    #
    # Add instrument to albedo module
    #
    for k in range(n_CSS):
        albModule.addInstrumentConfig(CSS[k].fov, CSS[k].nHat_B, CSS[k].r_PB_B)
        CSS[k].albedoInMsg.subscribeTo(albModule.albOutMsgs[k])
    
    #
    # Add albedo and CSS to task and setup logging before the simulation is initialized
    #
    scSim.AddModelToTask(simTaskName, albModule)
    scSim.AddModelToTask(simTaskName, CSS1)
    scSim.AddModelToTask(simTaskName, CSS2)
    scSim.AddModelToTask(simTaskName, CSS3)
    scSim.AddModelToTask(simTaskName, CSS4)
    scSim.AddModelToTask(simTaskName, CSS5)
    scSim.AddModelToTask(simTaskName, CSS6)
    css1Log = CSS1.cssDataOutMsg.recorder()
    scSim.AddModelToTask(simTaskName, css1Log)

    # setup logging
    dataLog = scObject.scStateOutMsg.recorder()
    scSim.AddModelToTask(simTaskName, dataLog)

    sunLog = sunMsg.recorder()
    scSim.AddModelToTask(simTaskName, sunLog)

    alb0Log = albModule.albOutMsgs[0].recorder()
    alb1Log = albModule.albOutMsgs[1].recorder()
    alb2Log = albModule.albOutMsgs[2].recorder()
    alb3Log = albModule.albOutMsgs[3].recorder()
    alb4Log = albModule.albOutMsgs[4].recorder()
    alb5Log = albModule.albOutMsgs[5].recorder()
    scSim.AddModelToTask(simTaskName, alb0Log)
    scSim.AddModelToTask(simTaskName, alb1Log)
    scSim.AddModelToTask(simTaskName, alb2Log)
    scSim.AddModelToTask(simTaskName, alb3Log)
    scSim.AddModelToTask(simTaskName, alb4Log)
    scSim.AddModelToTask(simTaskName, alb5Log)
    #
    # Initialize Simulation
    #
    scSim.InitializeSimulation()
    #
    scSim.ConfigureStopTime(simulationTime)
    scSim.ExecuteSimulation()
    #
    # Retrieve the logged data
    #
    n = int(simulationTime / simulationTimeStep + 1)
    dataCSS = np.zeros(shape=(n, 2))
    dataAlb = np.zeros(shape=(n, n_CSS))
    posData = dataLog.r_BN_N
    sunPosData = sunLog.PositionVector
    dataCSS[:, 0] = css1Log.OutputData
    dataAlb[:, 0] = alb0Log.AfluxAtInstrument
    dataAlb[:, 1] = alb1Log.AfluxAtInstrument
    dataAlb[:, 2] = alb2Log.AfluxAtInstrument
    dataAlb[:, 3] = alb3Log.AfluxAtInstrument
    dataAlb[:, 4] = alb4Log.AfluxAtInstrument
    dataAlb[:, 5] = alb5Log.AfluxAtInstrument

    np.set_printoptions(precision=16)

    # save albedo data
    save_albedo_data = pd.DataFrame(dataAlb.transpose())
    save_albedo_data.to_csv("albedo_test/albedo_power_basilisk.csv", header=False, index=False)

    # save sat positions
    save_sat_pos = pd.DataFrame(posData)
    save_sat_pos.to_csv("albedo_test/r_sat_basilisk.csv", header=False, index=False)

    # save sun positions
    save_sun_pos = pd.DataFrame(sunPosData)
    save_sun_pos.to_csv("albedo_test/r_sun_basilisk.csv", header=False, index=False)

    #
    # Plot the results
    #
    plt.close("all")  # clears out plots from earlier test runs
    plt.figure(1)
    timeAxis = dataLog.times()
    plt.plot(timeAxis * macros.NANO2SEC, np.sum(dataAlb, axis=1),
                linewidth=2, alpha=0.7, color=unitTestSupport.getLineColor(0, 2),
                label='Alb$_{1}$')

    plt.legend(loc='upper right')
    plt.xlabel('Time [s]')
    plt.ylabel('Albedo flux [W/m^2]')
    figureList = {}
    pltName = fileNameString + str(1) + str(int(albedoData)) + str(int(multipleInstrument)) + str(
        int(multiplePlanet)) + str(
        int(useEclipse))
    figureList[pltName] = plt.figure(1)

    if albedoData:
        filePath = os.path.abspath(dataPath + '/' + fileName)
        ALB1 = np.genfromtxt(filePath, delimiter=',')
        # ALB coefficient figures
        fig = plt.figure(2)
        ax = fig.add_subplot(111)
        ax.set_title('Earth Albedo Coefficients (All Sky)')
        ax.set(xlabel='Longitude (deg)', ylabel='Latitude (deg)')
        plt.imshow(ALB1, cmap='Reds', interpolation='none', extent=[-180, 180, 90, -90])
        plt.colorbar(orientation='vertical')
        ax.set_ylim(ax.get_ylim()[::-1])
        pltName = fileNameString + str(2) + str(int(albedoData)) + str(int(multipleInstrument)) + str(
            int(multiplePlanet)) + str(
            int(useEclipse))
        figureList[pltName] = plt.figure(2)

    if show_plots:
        plt.show()
    # close the plots being saved off to avoid over-writing old and new figures
    plt.close("all")
    return figureList


#
# This statement below ensures that the unit test scrip can be run as a
# stand-along python script
#
if __name__ == "__main__":
    run(
        True,  # show_plots
        True,  # albedoData
        False,  # multipleInstrument
        False,  # multiplePlanet
        True,  # useEclipse
        10  # simTimeStep
    )
